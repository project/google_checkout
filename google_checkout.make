; Drush Make (http://drupal.org/project/drush_make)
api = 2
core = 7.x

projects[google_checkout][type] = library
; projects[google_checkout][download][type] = file
; projects[google_checkout][download][sha1] = 2e3b9625cb85f057937c2d2ae4264a40f6e70486
; projects[google_checkout][download][url] = http://google-checkout-php-sample-code.googlecode.com/files/checkout-php-1.3.1.zip

projects[google_checkout][download][type] = svn
projects[google_checkout][download][url] = http://google-checkout-php-sample-code.googlecode.com/svn/trunk/

projects[libraries] = 2
